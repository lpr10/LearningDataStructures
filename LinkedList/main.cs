﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LinkedList
{
    class program
    {
        public static void Main()
        {
            LinkedListNode<int> five = new LinkedListNode<int>(5);
            LinkedListNode<int> ten = new LinkedListNode<int>(10);
            LinkedListNode<int> fifteen = new LinkedListNode<int>(15);
            LinkedListNode<int> twenty = new LinkedListNode<int>(20);

            var i = new LinkedList<LinkedListNode<int>>();

            i.AddFirst(five);
            i.AddFirst(ten);
            i.AddFirst(fifteen);
            i.AddLast(twenty);

            printOut(i);

            i.Remove(fifteen);

            printOut(i);


        }

        private static void printOut(LinkedList<LinkedListNode<int>> i)
        {
            
            var current = i.Head;
            while(current != null)
            {
                Console.WriteLine($"{current.Value.Value}");
                current = current.Next;

            }


        }
    }
}
