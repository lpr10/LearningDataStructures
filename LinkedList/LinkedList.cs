﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinkedList
{
    public class LinkedList<T> : System.Collections.Generic.ICollection<T>
    {

        /// <summary>
        /// The first node in the list or null if empty
        /// </summary>
        public LinkedListNode<T> Head { get; private set; }

        /// <summary>
        /// The last node in the list or null if empty
        /// </summary>
        public LinkedListNode<T> Tail { get; private set; }

        #region Add
        public void AddFirst(T value)
        {
            AddFirst(new LinkedListNode<T>(value));
        }

        public void AddFirst(LinkedListNode<T> node)
        {
            LinkedListNode<T> tempHead = Head;
                        
            Head = node;

            Head.Next = tempHead;

            Count++;

            if(Count == 1)
            {
                Tail = node;
            }
        }

        public void AddLast(T value)
        {
            AddLast(new LinkedListNode<T>(value)); 
        }

        public void AddLast(LinkedListNode<T> node)
        {
            //copy over temp tail -- dont need to do this
            LinkedListNode<T> tempTail = Tail;
            if (Count == 0)
            {
                Head = node;
            }
            else {
                //previous last tail had no next but now it does
                tempTail.Next = node;
            }
            //set this new node as the new tail
            Tail = node;

            //increase the count
            Count++;
        }

        #endregion

        #region Remove
        public bool Remove(T item)
        {
            LinkedListNode<T> current = Head;
            LinkedListNode<T> previous = null;

            while (current != null)
            {
                LinkedListNode<T> tempNext = current;

                if (current.Value.Equals(item))
                {
                    if (previous != null)
                    {
                        previous.Next = tempNext.Next;
                        return true;
                    }
                    else
                    {
                        Head = current.Next;
                        return true;
                    }

                }
                previous = current;
                current = current.Next;
                
            }

            return false;
        }
        #endregion

        #region ICollection 

        public void Add(T item)
        {
            AddFirst(item);
        }
        public int Count { get; private set; }
        /// <summary>
        /// This collection is never going to be fase, just return static value
        /// </summary>
        public bool IsReadOnly { get { return false; } }
        public void Clear()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Returns true if the item is in the list
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Contains(T item)
        {
            LinkedListNode<T> current = Head;
            while(current != null)
            {
                if(current.Value.Equals(item))
                {
                    return true;
                }
                current = current.Next;
            }
            return false;
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            LinkedListNode<T> current = Head;
            while (current != null)
            {
                array[arrayIndex++] = current.Value;
                current = current.Next;
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            throw new NotImplementedException();
        }



        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
        #endregion





    }
}
