﻿using System;
using System.Collections.Generic;

namespace LearningDataScructures
{

    public class Node
    {
        public int Value { get; set; }
        public Node Next { get; set; }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Node first = new Node() { Value = 5 };

            Node second = new Node() { Value = 9 };
            first.Next = second;
            Node third = new Node() { Value = 19 };
            second.Next = third;


            printNode(first);

        }

        private static void printNode(Node n)
        {
            while(n != null)
            {
                Console.WriteLine($"Value:{n.Value}, Node:{(n.Next != null ? n.Next.Value : 0)}");
                n = n.Next;
            }
            
        }
    }
}
